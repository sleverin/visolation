package session

import "visolation/internal/utils"

const (
	CookieName = "sessionID"
)

type sessionData struct {
	UserName string
}

type Session struct {
	data map[string]*sessionData
}

var InMemorySession *Session

func NewSession() *Session {
	s := new(Session)
	s.data = make(map[string]*sessionData)
	return s
}

func (s *Session) Init(userName string) string {
	sessionID := utils.GenerateID()

	data := &sessionData{UserName: userName}
	s.data[sessionID] = data
	return sessionID
}

func (s *Session) Get(sessionID string) string {
	data := s.data[sessionID]

	if data == nil {
		return ""
	}

	return data.UserName
}
