package handlers

import (
	"fmt"
	"html/template"
	"net/http"
	"visolation/internal/models"
	"visolation/internal/session"
)

func Index(w http.ResponseWriter, r *http.Request) {
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)

	t, err := template.ParseFiles("../views/index.html", "../views/header.html", "../views/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	people := []models.Friend{}
	if currentUser != nil {
		people, err = currentUser.FindFriends()
		if err != nil {
			panic(err)
		}
	}
	data := struct {
		CurrentUser *models.User
		ActivePage  string
		People      []models.Friend
	}{
		currentUser,
		"index",
		people,
	}

	t.ExecuteTemplate(w, "index", data)
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)

	t, err := template.ParseFiles("../views/notfound.html", "../views/header.html", "../views/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	w.WriteHeader(http.StatusNotFound)

	data := struct {
		CurrentUser *models.User
		ActivePage  string
	}{
		currentUser,
		"",
	}

	t.ExecuteTemplate(w, "notfound", data)
}

func Error(w http.ResponseWriter, r *http.Request) {
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)

	t, err := template.ParseFiles("../views/error.html", "../views/header.html", "../views/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	w.WriteHeader(http.StatusInternalServerError)

	data := struct {
		CurrentUser *models.User
		ActivePage  string
	}{
		currentUser,
		"",
	}

	t.ExecuteTemplate(w, "error", data)
}

func About(w http.ResponseWriter, r *http.Request) {
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)

	t, err := template.ParseFiles("../views/about.html", "../views/header.html", "../views/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	data := struct {
		CurrentUser *models.User
		ActivePage  string
	}{
		currentUser,
		"about",
	}

	t.ExecuteTemplate(w, "about", data)
}
