package handlers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"visolation/internal/models"
	"visolation/internal/session"
	"visolation/internal/utils"
)

type responseJson struct {
	Success bool
	Data    interface{}
	Error   string
}

func Friends(w http.ResponseWriter, r *http.Request) {
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)
	if currentUser == nil {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	t, err := template.ParseFiles("../views/friends.html", "../views/header.html", "../views/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	friends := []models.Friend{}

	await := []models.Friend{}
	if currentUser != nil {
		friends, err = currentUser.ShowFriends()
		if err != nil {
			utils.Logger.Println("error: " + err.Error())
			Error(w, r)
			return
		}
		await, err = currentUser.ShowWaitingPeople()
		if err != nil {
			utils.Logger.Println("error: " + err.Error())
			Error(w, r)
			return
		}
	}

	data := struct {
		CurrentUser *models.User
		ActivePage  string
		Friends     []models.Friend
		Await       []models.Friend
	}{
		currentUser,
		"friends",
		friends,
		await,
	}

	t.ExecuteTemplate(w, "friends", data)
}

func AddFriends(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	type requestStructure struct {
		FriendID int64
	}

	response := responseJson{}
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)
	if currentUser == nil {
		utils.Logger.Println("error: Unauthorized")
		w.WriteHeader(http.StatusUnauthorized)
		response.Success = false
		response.Error = "Unauthorized"
		json.NewEncoder(w).Encode(response)
		return
	}

	decoder := json.NewDecoder(r.Body)
	request := requestStructure{}
	err := decoder.Decode(&request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response.Success = false
		response.Error = err.Error()
		json.NewEncoder(w).Encode(response)
		return
	}

	isSuccess, err := currentUser.AddToFriend(request.FriendID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		response.Success = false
		response.Error = err.Error()
		json.NewEncoder(w).Encode(response)
		return
	}

	response.Success = isSuccess
	json.NewEncoder(w).Encode(response)
}

func ApproveFriend(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	type requestStructure struct {
		FriendID int64
	}

	response := responseJson{}
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)
	if currentUser == nil {
		utils.Logger.Println("error: Unauthorized")
		w.WriteHeader(http.StatusUnauthorized)
		response.Success = false
		response.Error = "Unauthorized"
		json.NewEncoder(w).Encode(response)
		return
	}

	decoder := json.NewDecoder(r.Body)
	request := requestStructure{}
	err := decoder.Decode(&request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response.Success = false
		response.Error = err.Error()
		json.NewEncoder(w).Encode(response)
		return
	}

	isSuccess, err := currentUser.ApproveFriend(request.FriendID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		response.Success = false
		response.Error = err.Error()
		json.NewEncoder(w).Encode(response)
		return
	}

	response.Success = isSuccess
	json.NewEncoder(w).Encode(response)
}

func SearchFriend(w http.ResponseWriter, r *http.Request) {
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)
	if currentUser == nil {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	var firstName, lastName, city string
	var friends []models.Friend
	var isPost bool
	if r.Method == "POST" {
		isPost = true
		firstName = r.FormValue("firstname")
		lastName = r.FormValue("lastname")
		city = r.FormValue("city")

		var err error
		friends, err = models.GetFriendsByParams(city, firstName, lastName, currentUser.ID)
		if err != nil {
			fmt.Println(err.Error())
			fmt.Println("Не нащли нечего")
		}
	}

	t, err := template.ParseFiles("../views/search.html", "../views/header.html", "../views/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	data := struct {
		CurrentUser *models.User
		ActivePage  string
		Friends     []models.Friend
		FirstName   string
		LastName    string
		City        string
		IsPost      bool
	}{
		currentUser,
		"",
		friends,
		firstName,
		lastName,
		city,
		isPost,
	}

	t.ExecuteTemplate(w, "search", data)
}

func DeclineFriend(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	type requestStructure struct {
		FriendID int64
	}

	response := responseJson{}
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)
	if currentUser == nil {
		utils.Logger.Println("error: Unauthorized")
		w.WriteHeader(http.StatusUnauthorized)
		response.Success = false
		response.Error = "Unauthorized"
		json.NewEncoder(w).Encode(response)
		return
	}

	decoder := json.NewDecoder(r.Body)
	request := requestStructure{}
	err := decoder.Decode(&request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response.Success = false
		response.Error = err.Error()
		json.NewEncoder(w).Encode(response)
		return
	}

	isSuccess, err := currentUser.DeclineFriend(request.FriendID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		response.Success = false
		response.Error = err.Error()
		json.NewEncoder(w).Encode(response)
		return
	}

	response.Success = isSuccess
	json.NewEncoder(w).Encode(response)
}
