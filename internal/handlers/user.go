package handlers

import (
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"time"
	"visolation/internal/models"
	"visolation/internal/session"
	"visolation/internal/utils"
)

func Logout(w http.ResponseWriter, r *http.Request) {
	currentUser, _ := models.GetCurrentUser(r, session.InMemorySession)
	if currentUser == nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
	cookie := &http.Cookie{
		Name:    session.CookieName,
		Value:   "",
		Expires: time.Now().Add(-24 * time.Hour),
	}

	http.SetCookie(w, cookie)
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func Registration(w http.ResponseWriter, r *http.Request) {
	currentUser, err := models.GetCurrentUser(r, session.InMemorySession)
	if currentUser != nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

	var errors = map[string]string{}
	var user = new(models.User)
	var address = new(models.Address)
	user.Address = *address

	if r.Method == "POST" {
		user.FirstName = r.FormValue("firstname")
		user.LastName = r.FormValue("lastname")
		user.Email = r.FormValue("email")
		user.Password = r.FormValue("password")
		user.BirthDay = r.FormValue("day")
		user.BirthMonth = r.FormValue("month")
		user.BirthYear = r.FormValue("year")

		address.City = r.FormValue("city")
		address.Address = r.FormValue("address")
		user.Address = *address

		var userInterests []models.Interest
		for _, value := range r.Form["interests"] {
			interest := new(models.Interest)
			interestID, _ := strconv.ParseInt(value, 10, 64)
			interest.ID = interestID

			userInterests = append(userInterests, *interest)
		}
		user.Interests = userInterests

		_, err := user.Save()
		if err != nil {
			errorMessages := strings.Split(err.Error(), ";")
			//fmt.Println(errorMessages[0])
			//fmt.Printf("%+v\n", errorMessages)
			for _, value := range errorMessages {
				errorValues := strings.Split(strings.TrimSpace(value), ":")
				errors[strings.TrimSpace(errorValues[0])] = strings.TrimSpace(errorValues[1])
			}
		} else {
			sessionID := session.InMemorySession.Init(user.Email)
			cookie := &http.Cookie{
				Name:    session.CookieName,
				Value:   sessionID,
				Expires: time.Now().Add(24 * time.Hour),
			}

			http.SetCookie(w, cookie)
			http.Redirect(w, r, "/", http.StatusSeeOther)
		}
	}

	t, err := template.ParseFiles("../views/registration.html", "../views/header.html", "../views/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	interests, err := models.GetInterestList(user.Interests)
	if err != nil {
		panic(err)
	}

	days := utils.GetDayList()
	years := utils.GetYearList()
	months := utils.GetMonthList()

	data := struct {
		CurrentUser *models.User
		User        *models.User
		Interests   []models.Interest
		ActivePage  string
		Days        map[int]string
		Months      map[string]string
		Years       map[int]string
		Errors      map[string]string
	}{
		currentUser,
		user,
		interests,
		"login",
		days,
		months,
		years,
		errors,
	}

	t.ExecuteTemplate(w, "registration", data)
}

func Login(w http.ResponseWriter, r *http.Request) {
	currentUser, err := models.GetCurrentUser(r, session.InMemorySession)
	if currentUser != nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

	hasError := false
	if r.Method == "POST" {
		email := r.FormValue("email")
		password := r.FormValue("password")

		user, err := models.GetUserByEmail(email)
		passwordHash := utils.GenerateHash(password)

		if err == nil && user.Password == passwordHash {
			fmt.Println("Login and Password are correct")

			sessionID := session.InMemorySession.Init(email)
			cookie := &http.Cookie{
				Name:    session.CookieName,
				Value:   sessionID,
				Expires: time.Now().Add(24 * time.Hour),
			}

			http.SetCookie(w, cookie)
			http.Redirect(w, r, "/", http.StatusSeeOther)
		} else {
			hasError = true
		}
	}

	t, err := template.ParseFiles("../views/login.html", "../views/header.html", "../views/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	data := struct {
		CurrentUser *models.User
		HasError    bool
		ActivePage  string
	}{
		currentUser,
		hasError,
		"login",
	}

	t.ExecuteTemplate(w, "login", data)
}

func Profile(w http.ResponseWriter, r *http.Request) {
	currentUser, err := models.GetCurrentUser(r, session.InMemorySession)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

	var errors = map[string]string{}

	vars := mux.Vars(r)
	profileID, err := strconv.ParseInt(vars["id"], 10, 64)
	user, err := models.GetUserByID(profileID)
	if err != nil {
		NotFound(w, r)
		return
	}

	if r.Method == "POST" {
		user.FirstName = r.FormValue("firstname")
		user.LastName = r.FormValue("lastname")
		user.BirthDay = r.FormValue("day")
		user.BirthMonth = r.FormValue("month")
		user.BirthYear = r.FormValue("year")

		user.Address.City = r.FormValue("city")
		user.Address.Address = r.FormValue("address")

		var userInterests []models.Interest
		for _, value := range r.Form["interests"] {
			interest := new(models.Interest)
			interestID, _ := strconv.ParseInt(value, 10, 64)
			interest.ID = interestID

			userInterests = append(userInterests, *interest)
		}
		user.Interests = userInterests

		_, err := user.Update()
		if err != nil {
			errorMessages := strings.Split(err.Error(), ";")
			for _, value := range errorMessages {
				errorValues := strings.Split(strings.TrimSpace(value), ":")
				errors[strings.TrimSpace(errorValues[0])] = strings.TrimSpace(errorValues[1])
			}
		} else {
			http.Redirect(w, r, "/", http.StatusSeeOther)
		}
	}

	fmt.Println(errors)

	t, err := template.ParseFiles(
		"../views/profile.html",
		"../views/ownprofile.html",
		"../views/userprofile.html",
		"../views/header.html",
		"../views/footer.html",
	)
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	isOwner := false
	if currentUser != nil && currentUser.ID == profileID {
		isOwner = true
	}

	interests, _ := models.GetInterestList(user.Interests)

	days := utils.GetDayList()
	years := utils.GetYearList()
	months := utils.GetMonthList()

	data := struct {
		CurrentUser *models.User
		User        *models.User
		Interests   []models.Interest
		IsOwner     bool
		ActivePage  string
		Days        map[int]string
		Months      map[string]string
		Years       map[int]string
		Errors      map[string]string
	}{
		currentUser,
		user,
		interests,
		isOwner,
		"profile",
		days,
		months,
		years,
		errors,
	}

	t.ExecuteTemplate(w, "profile", data)
}
