package models

import (
	"fmt"
	"time"
	"visolation/internal/utils"
)

type Friend struct {
	ID, Age                         int64
	FirstName, LastName, Email      string
	City, Address                   string
	Status                          int64
	Interests                       []Interest
	BirthDate, CreatedAt, UpdatedAt string
}

func GetFriends(userID int64) ([]Friend, error) {
	res, err := utils.Db.Query("SELECT u.id, u.first_name, u.last_name, u.email, a.city, a.address, u2f.status, u.birth_date FROM `user2friend` AS u2f "+
		"LEFT JOIN `user` AS u ON u2f.friend_id = u.id "+
		"LEFT JOIN `address` AS a ON u.address_id = a.id "+
		"WHERE u2f.`user_id` = ? AND u2f.status != -1", userID)
	if err != nil {
		return nil, err
	}

	var friends []Friend
	for res.Next() {
		var friend Friend
		err := res.Scan(&friend.ID, &friend.FirstName, &friend.LastName, &friend.Email, &friend.City, &friend.Address, &friend.Status, &friend.BirthDate)
		if err != nil {
			return nil, err
		}

		// Распарсим дату рождения
		layout := "2006-01-02"
		t, err := time.Parse(layout, friend.BirthDate)

		if err != nil {
			fmt.Println(err)
		}

		friend.Age = int64(time.Now().Sub(t).Hours() / (24 * 365))
		friends = append(friends, friend)
	}

	return friends, nil
}

func FindFriends(userID int64, city string, birthDate string) ([]Friend, error) {
	// Распарсим дату рождения
	layout := "2006-01-02"
	t, err := time.Parse(layout, birthDate)

	if err != nil {
		fmt.Println(err)
	}

	startDate := t.AddDate(-5, 0, 0)
	endDate := t.AddDate(5, 0, 0)

	res, err := utils.Db.Query("SELECT u.id, u.first_name, u.last_name, u.email, a.city, a.address, u.birth_date FROM `user` AS u "+
		"LEFT JOIN user2friend AS u2f ON (u2f.user_id = ? AND u2f.friend_id = u.id) OR (u2f.user_id = u.id AND u2f.`friend_id` = ?) "+
		"LEFT JOIN address AS a ON u.address_id = a.id "+
		"WHERE u.id IN ("+
		"SELECT user_id FROM ("+
		"SELECT user_id, count(*) AS cnt FROM user2interest WHERE interest_id IN ("+
		"SELECT interest_id "+
		"FROM user2interest "+
		"WHERE user_id = ?) "+
		"AND user_id != ? "+
		"AND user_id != 0 "+
		"GROUP BY user_id HAVING cnt > 1 ORDER BY cnt DESC"+
		") AS t1) "+
		"AND u2f.id IS NULL "+
		"AND a.city = ? "+
		"AND birth_date > ? "+
		"AND birth_date < ? LIMIT 9",
		userID,
		userID,
		userID,
		userID,
		city,
		fmt.Sprintf("%d.%02d.%02d", startDate.Year(), startDate.Month(), startDate.Day()),
		fmt.Sprintf("%d.%02d.%02d", endDate.Year(), endDate.Month(), endDate.Day()),
	)

	if err != nil {
		return nil, err
	}

	defer res.Close()

	var friends []Friend
	for res.Next() {
		var friend Friend
		err := res.Scan(&friend.ID, &friend.FirstName, &friend.LastName, &friend.Email, &friend.City, &friend.Address, &friend.BirthDate)
		if err != nil {
			panic(err)
		}

		// Распарсим дату рождения
		layout := "2006-01-02"
		t, err := time.Parse(layout, friend.BirthDate)

		if err != nil {
			fmt.Println(err)
		}

		friend.Age = int64(time.Now().Sub(t).Hours() / (24 * 365))
		friends = append(friends, friend)
	}

	return friends, nil
}

func GetFriendsByParams(city, firstName, lastName string, userId int64) ([]Friend, error) {
	res, err := utils.Db.Query("SELECT u.id, u.first_name, u.last_name, u.email, a.city, a.address, IFNULL(MAX(u2f.status), -2) as status, u.birth_date FROM `user` AS u "+
		"LEFT JOIN `address` AS a ON u.address_id = a.id "+
		"LEFT JOIN `user2friend` AS u2f ON u.id = u2f.friend_id "+
		"WHERE u.`first_name` LIKE ? AND u.`last_name` LIKE ? AND u.id != ? GROUP BY u.id ORDER BY u.id ASC", firstName+"%", lastName+"%", userId)

	if err != nil {
		return nil, err
	}

	var friends []Friend
	for res.Next() {
		var friend Friend
		err := res.Scan(&friend.ID, &friend.FirstName, &friend.LastName, &friend.Email, &friend.City, &friend.Address, &friend.Status, &friend.BirthDate)
		if err != nil {
			return nil, err
		}

		// Распарсим дату рождения
		layout := "2006-01-02"
		t, err := time.Parse(layout, friend.BirthDate)

		if err != nil {
			fmt.Println(err)
		}

		friend.Age = int64(time.Now().Sub(t).Hours() / (24 * 365))
		friends = append(friends, friend)
	}

	return friends, nil
}

func GetWaitingApprovalFriends(userID int64) ([]Friend, error) {
	res, err := utils.Db.Query("SELECT u.id, u.first_name, u.last_name, u.email, a.city, a.address, u2f.status, u.birth_date FROM `user2friend` AS u2f "+
		"LEFT JOIN `user` AS u ON u2f.user_id = u.id "+
		"LEFT JOIN `address` AS a ON u.address_id = a.id "+
		"WHERE u2f.`friend_id` = ? AND u2f.`status` = 0", userID)
	if err != nil {
		return nil, err
	}

	var friends []Friend
	for res.Next() {
		var friend Friend
		err := res.Scan(&friend.ID, &friend.FirstName, &friend.LastName, &friend.Email, &friend.City, &friend.Address, &friend.Status, &friend.BirthDate)
		if err != nil {
			return nil, err
		}

		// Распарсим дату рождения
		layout := "2006-01-02"
		t, err := time.Parse(layout, friend.BirthDate)

		if err != nil {
			fmt.Println(err)
		}

		friend.Age = int64(time.Now().Sub(t).Hours() / (24 * 365))
		friends = append(friends, friend)
	}

	return friends, nil
}
