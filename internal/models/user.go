package models

import (
	"database/sql"
	"errors"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"net/http"
	"time"
	"visolation/internal/session"
	"visolation/internal/utils"
)

type User struct {
	ID, AddressID, Age                   int64
	FirstName, LastName, Email, Password string
	Address                              Address
	Interests                            []Interest
	BirthDay, BirthMonth, BirthYear      string
	BirthDate, CreatedAt, UpdatedAt      string
}

type User2Friend struct {
	ID       int64
	FriendID int64
	Friend   User
}

func GetUserByEmail(email string) (*User, error) {
	row := utils.Db.QueryRow("SELECT u.`id`, u.`first_name`, u.`last_name`, u.`email`, u.`password`, u.`birth_date`, u.`address_id`, u.`created_at`, u.`updated_at` "+
		"FROM `user` as u LEFT JOIN `address` as a ON u.`address_id` = a.`id` WHERE u.`email` = ? LIMIT 1", email)

	user := new(User)
	err := row.Scan(&user.ID, &user.FirstName, &user.LastName, &user.Email, &user.Password, &user.BirthDate, &user.AddressID, &user.CreatedAt, &user.UpdatedAt)
	if err == sql.ErrNoRows {
		return nil, err
	}

	// Получим адрес пользователя
	addressRow := utils.Db.QueryRow("SELECT `city`, `address` "+
		"FROM `address` WHERE `id` = ? LIMIT 1", user.AddressID)
	address := new(Address)
	err = addressRow.Scan(&address.City, &address.Address)
	if err == sql.ErrNoRows {
		return nil, err
	}
	user.Address = *address

	return user, nil
}

func GetUserByID(id int64) (*User, error) {
	userRow := utils.Db.QueryRow("SELECT u.`id`, u.`first_name`, u.`last_name`, u.`email`, u.`password`, u.`birth_date`, u.`address_id`, u.`created_at`, u.`updated_at` "+
		"FROM `user` as u WHERE u.`id` = ? LIMIT 1", id)

	user := new(User)
	err := userRow.Scan(
		&user.ID,
		&user.FirstName,
		&user.LastName,
		&user.Email,
		&user.Password,
		&user.BirthDate,
		&user.AddressID,
		&user.CreatedAt,
		&user.UpdatedAt,
	)
	if err == sql.ErrNoRows {
		return nil, err
	}

	// Получим адрес пользователя
	addressRow := utils.Db.QueryRow("SELECT `city`, `address` "+
		"FROM `address` WHERE `id` = ? LIMIT 1", user.AddressID)
	address := new(Address)
	err = addressRow.Scan(&address.City, &address.Address)
	if err == sql.ErrNoRows {
		return nil, err
	}
	user.Address = *address

	// Распарсим дату рождения
	layout := "2006-01-02"
	t, err := time.Parse(layout, user.BirthDate)

	if err != nil {
		fmt.Println(err)
	}

	user.BirthDate = fmt.Sprintf("%02d.%02d.%d", t.Day(), t.Month(), t.Year())
	user.BirthDay = fmt.Sprintf("%02d", t.Day())
	user.BirthMonth = fmt.Sprintf("%02d", t.Month())
	user.BirthYear = fmt.Sprintf("%d", t.Year())
	user.Age = int64(time.Now().Sub(t).Hours() / (24 * 365))

	// Получим интересы пользователя
	res, err := utils.Db.Query("SELECT i.`id`, i.`title`, i.`sort_order`, i.`status`, i.`created_at`, i.`updated_at` "+
		"FROM `user2interest` AS u2i "+
		"JOIN `interest` AS i ON u2i.interest_id = i.id "+
		"WHERE u2i.user_id = ? AND i.`status` = '1' ORDER BY i.`sort_order`", id)

	if err != nil {
		return nil, err
	}

	defer res.Close()

	interests := []Interest{}
	for res.Next() {
		var intr Interest
		err := res.Scan(&intr.ID, &intr.Title, &intr.SortOrder, &intr.Status, &intr.CreatedAt, &intr.UpdatedAt)
		if err != nil {
			panic(err)
		}

		interests = append(interests, intr)
	}
	user.Interests = interests

	return user, nil
}

func GetCurrentUser(r *http.Request, s *session.Session) (*User, error) {
	cookie, _ := r.Cookie(session.CookieName)
	if cookie != nil {
		email := s.Get(cookie.Value)
		user, _ := GetUserByEmail(email)
		if user != nil {
			return user, nil
		}
	}

	return nil, errors.New("not found")
}

func (u *User) validate(isNew bool) error {
	err := validation.ValidateStruct(
		u,
		validation.Field(&u.Email, validation.Required, is.Email),
		validation.Field(&u.Password, validation.Required, validation.Length(5, 255)),
		validation.Field(&u.FirstName, validation.Required, validation.Length(2, 32)),
		validation.Field(&u.LastName, validation.Required, validation.Length(2, 32)),
		validation.Field(&u.BirthDay, validation.Required),
		validation.Field(&u.BirthMonth, validation.Required),
		validation.Field(&u.BirthYear, validation.Required),
	)

	if err != nil {
		return err
	}

	if isNew {
		rows, err := utils.Db.Query("SELECT COUNT(*) FROM `user` WHERE `email` = ?", u.Email)
		if err != nil {
			return err
		}
		defer rows.Close()

		var count int
		for rows.Next() {
			if err := rows.Scan(&count); err != nil {
				return err
			}
		}

		if count > 0 {
			return errors.New("Email: Данный email уже зарегистрирован в системе")
		}
	}

	return nil
}

func (u *User) Save() (int64, error) {
	err := u.validate(true)
	if err != nil {
		return 0, err
	}

	u.BirthDate = fmt.Sprintf("%s-%s-%s", u.BirthYear, u.BirthMonth, u.BirthDay)
	u.Password = utils.GenerateHash(u.Password)

	addressID, err := u.Address.Save()
	if err != nil {
		return 0, err
	}

	query, err := utils.Db.Prepare("INSERT INTO `user` (`first_name`, `last_name`, `birth_date`, `email`, `password`, `address_id`) VALUES (?, ?, ?, ?, ?, ?)")
	if err != nil {
		panic(err)
	}

	res, err := query.Exec(u.FirstName, u.LastName, u.BirthDate, u.Email, u.Password, addressID)
	if err != nil {
		panic(err)
	}

	userID, err := res.LastInsertId()
	if err != nil {
		panic(err)
	}

	for _, value := range u.Interests {
		_, err := utils.Db.Query("INSERT INTO `user2interest` (`user_id`, `interest_id`) VALUES (?, ?)", userID, value.ID)
		if err != nil {
			panic(err)
		}
	}

	return userID, nil
}

func (u *User) Update() (int64, error) {
	err := u.validate(false)
	if err != nil {
		return 0, err
	}

	u.BirthDate = fmt.Sprintf("%s-%s-%s", u.BirthYear, u.BirthMonth, u.BirthDay)

	addressID, err := u.Address.Save()
	if err != nil {
		return 0, err
	}

	query, err := utils.Db.Prepare("UPDATE `user` SET `first_name` =?, `last_name` = ?, `birth_date` = ?, `address_id` = ?, `updated_at`=now() WHERE `id` = ?")
	if err != nil {
		panic(err)
	}

	_, err = query.Exec(u.FirstName, u.LastName, u.BirthDate, addressID, u.ID)
	if err != nil {
		panic(err)
	}

	_, err = utils.Db.Exec("DELETE FROM `user2interest` WHERE `user_id` = ?", u.ID)
	if err != nil {
		panic(err)
	}

	for _, value := range u.Interests {
		_, err := utils.Db.Query("INSERT INTO `user2interest` (`user_id`, `interest_id`) VALUES (?, ?)", u.ID, value.ID)
		if err != nil {
			panic(err)
		}
	}

	return u.ID, nil
}

func (u *User) FindFriends() ([]Friend, error) {
	return FindFriends(u.ID, u.Address.City, u.BirthDate)
}

func (u *User) ShowFriends() ([]Friend, error) {
	return GetFriends(u.ID)
}

func (u *User) ShowWaitingPeople() ([]Friend, error) {
	return GetWaitingApprovalFriends(u.ID)
}

func (u *User) AddToFriend(friendID int64) (bool, error) {
	row := utils.Db.QueryRow("SELECT `user_id`, `friend_id` FROM `user2friend` WHERE `user_id` = ? AND friend_id = ?", u.ID, friendID)

	u2f := new(User2Friend)
	err := row.Scan(
		&u2f.ID,
		&u2f.FriendID,
	)
	if err == nil {
		return false, errors.New("This friend already exists in your list")
	}

	_, err = utils.Db.Query("INSERT INTO `user2friend` (`user_id`, `friend_id`, `status`) VALUES (?, ?, 0)", u.ID, friendID)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (u *User) ApproveFriend(friendID int64) (bool, error) {
	res, err := utils.Db.Query("UPDATE `user2friend` SET `status` = 1 WHERE `user_id` = ? AND friend_id = ?", friendID, u.ID)
	if err != nil {
		return false, err
	}
	defer res.Close()

	res, err = utils.Db.Query("INSERT INTO `user2friend` (`user_id`, `friend_id`, `status`) VALUES (?, ?, 1)", u.ID, friendID)
	if err != nil {
		return false, err
	}
	defer res.Close()

	return true, nil
}

func (u *User) DeclineFriend(friendID int64) (bool, error) {
	_, err := utils.Db.Query("UPDATE `user2friend` SET `status` = -1 WHERE `user_id` = ? AND friend_id = ?", friendID, u.ID)
	if err != nil {
		return false, err
	}

	return true, nil
}
