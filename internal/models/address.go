package models

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"visolation/internal/utils"
)

type Address struct {
	ID                                  uint16
	City, Address, CreatedAt, UpdatedAt string
}

func (a *Address) validate() error {
	return validation.ValidateStruct(
		a, validation.Field(&a.City, validation.Required),
	)
}

func (a *Address) Save() (int64, error) {
	query, err := utils.Db.Prepare("INSERT INTO `address` (`city`, `address`) VALUES (?, ?)")
	if err != nil {
		panic(err)
	}

	res, err := query.Exec(a.City, a.Address)
	if err != nil {
		panic(err)
	}

	addressID, err := res.LastInsertId()
	if err != nil {
		panic(err)
	}

	return addressID, nil
}
