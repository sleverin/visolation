package models

import (
	"visolation/internal/utils"
)

type Interest struct {
	ID                   int64
	Title                string
	SortOrder            uint16
	Status               uint16
	IsChecked            bool
	CreatedAt, UpdatedAt string
}

func GetInterestList(userInterests []Interest) ([]Interest, error) {
	res, err := utils.Db.Query("SELECT id, title, sort_order, status, created_at, updated_at FROM interest WHERE status = '1'")
	if err != nil {
		return nil, err
	}

	defer res.Close()

	var interests []Interest
	for res.Next() {
		var interest Interest
		err := res.Scan(&interest.ID, &interest.Title, &interest.SortOrder, &interest.Status, &interest.CreatedAt, &interest.UpdatedAt)
		if err != nil {
			panic(err)
		}

		for _, userInterest := range userInterests {
			if interest.ID == userInterest.ID {
				interest.IsChecked = true
			}
		}
		interests = append(interests, interest)
	}

	return interests, nil
}
