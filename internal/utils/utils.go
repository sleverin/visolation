package utils

import (
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"strconv"
)

func GenerateID() string {
	b := make([]byte, 16)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func GenerateHash(hashedString string) string {
	hash := md5.New()
	hash.Write([]byte(hashedString))
	return hex.EncodeToString(hash.Sum(nil))
}

func GetDayList() map[int]string {
	days := map[int]string{}

	for i := 1; i <= 31; i++ {
		day := strconv.Itoa(i)
		if i < 10 {
			day = "0" + day
		}
		days[i] = day
	}

	return days
}

func GetYearList() map[int]string {
	years := map[int]string{}

	for i := 1950; i <= 2021; i++ {
		years[i] = strconv.Itoa(i)
	}

	return years
}

func GetMonthList() map[string]string {
	months := map[string]string{}

	months["01"] = "Январь"
	months["02"] = "Февраль"
	months["03"] = "Март"
	months["04"] = "Апрель"
	months["05"] = "Май"
	months["06"] = "Июнь"
	months["07"] = "Июль"
	months["08"] = "Август"
	months["09"] = "Сентябрь"
	months["10"] = "Октябрь"
	months["11"] = "Ноябрь"
	months["12"] = "Декабрь"

	return months
}
