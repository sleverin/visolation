package utils

import "database/sql"

const (
	DbConnString = "root:root@tcp(127.0.0.1:3306)/visolation"
)

var Db *sql.DB

func InitDB(dataSourceName string) error {
	var err error

	Db, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		return err
	}

	Db.SetMaxOpenConns(1000)

	return Db.Ping()
}
