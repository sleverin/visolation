package main

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"visolation/internal/handlers"
	"visolation/internal/session"
	"visolation/internal/utils"
)

func handleFunc() {
	r := mux.NewRouter()

	r.HandleFunc("/", handlers.Index).Methods("get")
	r.HandleFunc("/registration", handlers.Registration).Methods("GET", "POST")
	r.HandleFunc("/login", handlers.Login).Methods("GET", "POST")
	r.HandleFunc("/logout", handlers.Logout).Methods("GET")
	r.HandleFunc("/about", handlers.About).Methods("GET")
	r.HandleFunc("/friends", handlers.Friends).Methods("GET")
	r.HandleFunc("/add-to-friends", handlers.AddFriends).Methods("POST")
	r.HandleFunc("/approve-friends", handlers.ApproveFriend).Methods("POST")
	r.HandleFunc("/search-friends", handlers.SearchFriend).Methods("GET", "POST")
	r.HandleFunc("/decline-friends", handlers.DeclineFriend).Methods("POST")
	r.HandleFunc("/profile/{id:[0-9]+}", handlers.Profile)

	r.NotFoundHandler = http.HandlerFunc(handlers.NotFound)

	http.Handle("/web/", http.StripPrefix("/web/", http.FileServer(http.Dir("../web/"))))
	http.Handle("/", r)

	http.ListenAndServe(":8000", nil)
}

func Recovering() {
	if r := recover(); r != nil {
		utils.Logger.Println("Recovering from panic:", r)
	}
}

func main() {
	f, _ := os.OpenFile("app.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer f.Close()

	utils.Logger = log.New(f, "Logger: ", log.LstdFlags)

	err := utils.InitDB(utils.DbConnString)
	if err != nil {
		log.Fatal(err)
	}

	session.InMemorySession = session.NewSession()
	defer Recovering()

	handleFunc()
}
