
window.onload = function () {

    let peopleButtons = document.getElementsByClassName('add-to-friends');
    for (let i = 0; i < peopleButtons.length; i++) {
        let button = peopleButtons[i];
        button.onclick = function () {
            let request = {
                "FriendID": parseInt(this.dataset.userid)
            };
            fetch("/add-to-friends",
                {
                    method: "POST",
                    body: JSON.stringify(request)
                })
                .then(response => {
                    if (response.status !== 200) {
                        return Promise.reject();
                    }
                    return response.json();
                })
                .then(function (data) {
                    if (data.Success) {
                        alert("Запрос успешно отправлен");
                        let col = document.getElementById("people-id-" + request.FriendID);
                        col.remove();
                    }
                    console.log(data)
                    console.log(JSON.stringify(data))
                })
                .catch(() => {
                    alert("Данные пользователь уже у вас в друзьях")
                });
        }
    }

    let approveButtons = document.getElementsByClassName('approve-friend');
    for (let i = 0; i < approveButtons.length; i++) {
        let button = approveButtons[i];
        button.onclick = function () {
            let request = {
                "FriendID": parseInt(this.dataset.userid)
            };
            fetch("/approve-friends",
                {
                    method: "POST",
                    body: JSON.stringify(request)
                })
                .then(response => {
                    if (response.status !== 200) {
                        return Promise.reject();
                    }
                    return response.json();
                })
                .then(function (data) {
                    if (data.Success) {
                        alert("Запрос успешно отправлен");
                        let col = document.getElementById("await-id-" + request.FriendID);
                        col.remove();
                    }
                    console.log(data)
                    console.log(JSON.stringify(data))
                })
                .catch(() => console.log('ошибка'));
        }
    }

    let declineButtons = document.getElementsByClassName('decline-friend');
    for (let i = 0; i < declineButtons.length; i++) {
        let button = declineButtons[i];
        button.onclick = function () {
            let request = {
                "FriendID": parseInt(this.dataset.userid)
            };
            fetch("/decline-friends",
                {
                    method: "POST",
                    body: JSON.stringify(request)
                })
                .then(response => {
                    if (response.status !== 200) {
                        return Promise.reject();
                    }
                    return response.json();
                })
                .then(function (data) {
                    if (data.Success) {
                        alert("Запрос успешно отправлен");
                        let col = document.getElementById("await-id-" + request.FriendID);
                        col.remove();
                    }
                    console.log(data)
                    console.log(JSON.stringify(data))
                })
                .catch(() => console.log('ошибка'));
        }
    }
}